FROM ubuntu:20.04
MAINTAINER Dimimon

ENV DEBIAN_Frondend=noninteractive

RUN apt update && apt upgrade -y

RUN apt install -y wget iproute2 software-properties-common language-pack-en jq
#RUN apt install xinetd
RUN apt install tzdata
RUN apt install software-properties-common
#RUN add repository ppa:ondrej/php
RUN apt-get install gettext -y
RUN apt update && apt-get install php7.4 -y
RUN apt install -y libapache2-mod-php7.4
RUN apt install apache2
RUN apt install patch


ENV VERSION=1.5.9


RUN wget https://github.com/FOGProject/fogproject/archive/${VERSION}.tar.gz \
 && tar xvfz ${VERSION}.tar.gz \
 && cd fogproject-${VERSION}/bin \
 && mkdir -p /backup \
 && bash ./installfog.sh  --autoaccept --no-systemd

# remove obsolet pifdiles
#RUN rm -f /var/run/fog/FOG*

# force redirect to FOG root URL from Apache base URL's
COPY assets/index.php /var/www
COPY assets/index.php /var/www/html
RUN rm /var/www/html/index.html


# patch vsftpd init file because start with failure
ADD assets/vsftpd.patch .
RUN patch -r /etc/init.d/vsftpd vsftpd.patch && rm -f vsftpd.patch

# remove FOG sources
RUN rm -rf /fogproject-* /${VERSION}.tar.gz


# saving default data
RUN mkdir -p /opt/fog/default/
RUN mkdir -p /opt/fog/default/var/lib/mysql
RUN mkdir -p /opt/fog/default/images
#RUN cp -rp /var/lib/mysql /opt/fog/default/
#RUN cp -rp /images /opt/fog/default/

RUN touch /INIT
ADD assets/entry.sh .
CMD bash entry.sh
